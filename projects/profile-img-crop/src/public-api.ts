/*
 * Public API Surface of profile-img-crop
 */

export * from './lib/core/profile-img-crop.service';
export * from './lib/feature/profile-img-crop.component';
