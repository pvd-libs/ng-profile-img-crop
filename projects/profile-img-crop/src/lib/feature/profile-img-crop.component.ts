import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  inject,
  Input,
  OnDestroy,
  Output,
  Renderer2,
  ViewChild,
} from '@angular/core';
import {NgClass, NgIf} from '@angular/common';
import {FormControl, ReactiveFormsModule} from '@angular/forms';
import {Subject, takeUntil} from 'rxjs';
import {MousePosition} from '../core/@types';

@Component({
  selector: 'ngc-img-crop',
  templateUrl: './profile-img-crop.component.html',
  styleUrls: ['./profile-img-crop.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [NgIf, ReactiveFormsModule, NgClass],
  providers: [],
})
export class ProfileImgCropComponent implements AfterViewInit, OnDestroy {
  @Input() file: File | null = null;
  @Input() rangeLabel: string = 'Zoom:';
  @Input() cropLabel: string = 'Crop';
  @Input() cropBtnStyleClasses: string[] = [];
  @Input() canvasContainerStyleClasses: string[] = [];
  @Input() cropAreaStyleClasses: string[] = [];
  @Input() canvasStyleClasses: string[] = [];
  @Input() controlContainerStyleClasses: string[] = [];
  @Input() primaryColor: string = '#2497E3';
  @Input() secondaryColor: string = '#A1D0FF';

  @Output() fileEdited: EventEmitter<File> = new EventEmitter<File>();

  @ViewChild('cropBtn') cropBtn!: ElementRef<HTMLButtonElement>;
  @ViewChild('zoomRange') zoomRange!: ElementRef<HTMLInputElement>;
  @ViewChild('cropArea') cropArea!: ElementRef<HTMLDivElement>;
  @ViewChild('canvasContainer') canvasContainer!: ElementRef<HTMLDivElement>;
  @ViewChild('canvas') canvas!: ElementRef<HTMLCanvasElement>;
  @ViewChild('controlContainer') controlContainer!: ElementRef<HTMLDivElement>;

  readonly cropAreaId: string = 'crop-area';
  readonly INITIAL_ZOOM_LEVEL: number = 1.5;

  private renderer: Renderer2 = inject(Renderer2);
  private readonly ZOOM_MULTIPLY_RATIO: number = 200;

  private _destroy: Subject<void> = new Subject<void>();
  private __zoomLevel = this.INITIAL_ZOOM_LEVEL;
  private __cropAreaLeft?: number;
  private __cropAreaTop?: number;
  private __isDragging = false;
  private img: HTMLImageElement | null = null;

  private maxImageSize: number = 700;

  zoomControl: FormControl<number | null> = new FormControl(this.INITIAL_ZOOM_LEVEL);
  mousePosition: MousePosition = {x: 0, y: 0};
  offset: number[] = [0, 0];

  ngAfterViewInit(): void {
    this.handleImage();

    this.zoomControl.valueChanges
      .pipe(takeUntil(this._destroy))
      .subscribe((value: number | null) => this.onZoom(value ?? this.INITIAL_ZOOM_LEVEL));
  }

  ngOnDestroy(): void {
    this._destroy.next();
    this._destroy.complete();
  }

  handleImage(): void {
    this.img = new Image();
    this.img.onload = this.onImageLoad.bind(this);

    const file: File = <File>this.file;
    const reader: FileReader = new FileReader();

    const onReaderLoad = (): void => {
      this.img!.src = reader.result as string;
    };

    reader.onload = onReaderLoad.bind(this);
    reader.readAsDataURL(file);
  }

  onImageLoad(): void {
    const canvasContainer: HTMLDivElement = this.canvasContainer.nativeElement;
    const canvas: HTMLCanvasElement = this.canvas.nativeElement;
    const ctx: CanvasRenderingContext2D | null = canvas.getContext('2d');
    const cropArea: HTMLDivElement = this.cropArea.nativeElement;
    const controlContainer: HTMLDivElement = this.controlContainer.nativeElement;

    let imgWidth: number = this.img!.width;
    let imgHeight: number = this.img!.height;

    if (imgWidth > this.maxImageSize) imgWidth = this.maxImageSize;
    if (imgHeight > this.maxImageSize) imgHeight = this.maxImageSize;

    this.renderer.setStyle(canvasContainer, 'max-width', `${imgWidth}px`);
    this.renderer.setStyle(canvasContainer, 'max-height', `${imgHeight}px`);
    this.renderer.setStyle(controlContainer, 'max-width', `${imgWidth}px`);
    this.renderer.setStyle(controlContainer, 'max-height', `${imgHeight}px`);
    this.renderer.setAttribute(canvas, 'width', `${imgWidth}`);
    this.renderer.setAttribute(canvas, 'height', `${imgHeight}`);

    this.setZoomLevel(cropArea, this.__zoomLevel);

    ctx!.drawImage(this.img!, 0, 0, imgWidth, imgHeight);

    this.__cropAreaLeft = parseFloat(cropArea.style.left);
    this.__cropAreaTop = parseFloat(cropArea.style.top);
  }

  onDragStart(e: MouseEvent | TouchEvent) {
    const target: HTMLElement = e.target as HTMLElement;

    if (target?.id != this.cropAreaId) return;

    const cropArea: HTMLDivElement = this.cropArea.nativeElement;
    this.__isDragging = true;

    this.offset =
      e instanceof MouseEvent
        ? [cropArea.offsetLeft - e.clientX, cropArea.offsetTop - e.clientY]
        : [cropArea.offsetLeft - e.touches[0].clientX, cropArea.offsetTop - e.touches[0].clientY];
  }

  onDragMove(e: MouseEvent | TouchEvent) {
    const target: HTMLElement = e.target as HTMLElement;
    if (target?.id != this.cropAreaId) return;

    e.preventDefault();

    if (this.__isDragging) {
      const cropArea: HTMLDivElement = this.cropArea.nativeElement;

      let clientX: number = e instanceof MouseEvent ? e.clientX : e.touches[0].clientX;
      let clientY: number = e instanceof MouseEvent ? e.clientY : e.touches[0].clientY;

      this.mousePosition = {
        x: clientX,
        y: clientY,
      };

      this.renderer.setStyle(cropArea, 'left', `${this.mousePosition.x + this.offset[0]}px`);
      this.renderer.setStyle(cropArea, 'top', `${this.mousePosition.y + this.offset[1]}px`);
    }
  }

  onDragEnd() {
    if (this.__isDragging) {
      this.__isDragging = false;
    }
  }

  crop() {
    const canvas = this.canvas.nativeElement;
    const cropArea = this.cropArea.nativeElement;

    const cropAreaRect = cropArea.getBoundingClientRect();
    const canvasRect = canvas.getBoundingClientRect();

    // Calculate the center of the crop area
    const centerX = cropAreaRect.left + cropAreaRect.width / 2 - canvasRect.left;
    const centerY = cropAreaRect.top + cropAreaRect.height / 2 - canvasRect.top;

    const diameter = parseInt(cropArea.style.width);

    const outputCanvas = document.createElement('canvas');
    outputCanvas.width = diameter;
    outputCanvas.height = diameter;

    const outputCtx = outputCanvas.getContext('2d');

    // Calculate the adjusted position and size based on the zoom level
    const adjustedX = centerX - diameter / 2;
    const adjustedY = centerY - diameter / 2;

    // Create a circular clipping path
    outputCtx?.beginPath();
    outputCtx?.arc(diameter / 2, diameter / 2, diameter / 2, 0, Math.PI * 2);
    outputCtx?.closePath();
    outputCtx?.clip();

    // Reverse the zoom effect for extraction
    outputCtx?.drawImage(
      canvas,
      adjustedX,
      adjustedY,
      diameter,
      diameter,
      0,
      0,
      diameter,
      diameter,
    );

    const editedFile: File = this.createFile(outputCanvas.toDataURL());
    this.fileEdited.emit(editedFile);
  }

  onZoom(zoomLevel: number) {
    this.__zoomLevel = zoomLevel;
    const cropArea: HTMLDivElement = this.cropArea.nativeElement;
    this.setZoomLevel(cropArea, zoomLevel);
  }

  setZoomLevel(cropArea: HTMLDivElement, zoomLevel: number) {
    this.renderer.setStyle(cropArea, 'width', `${this.ZOOM_MULTIPLY_RATIO * zoomLevel}px`);
    this.renderer.setStyle(cropArea, 'height', `${this.ZOOM_MULTIPLY_RATIO * zoomLevel}px`);
  }

  createFile(dataURI: string): File {
    const data: string = atob(dataURI.split(',')[1]);
    const arrayBuffer: ArrayBuffer = new ArrayBuffer(data.length);
    const view: Uint8Array = new Uint8Array(arrayBuffer);

    for (let i: number = 0; i < data.length; i++) {
      view[i] = data.charCodeAt(i);
    }

    const blob: Blob = new Blob([arrayBuffer], {type: 'image/png'});

    return new File([blob], 'output.png', {type: 'image/png', lastModified: Date.now()});
  }
}
