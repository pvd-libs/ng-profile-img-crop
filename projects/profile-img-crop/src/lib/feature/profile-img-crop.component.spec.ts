import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileImgCropComponent } from './profile-img-crop.component';

describe('ProfileImgCropComponent', () => {
  let component: ProfileImgCropComponent;
  let fixture: ComponentFixture<ProfileImgCropComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProfileImgCropComponent]
    });
    fixture = TestBed.createComponent(ProfileImgCropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
