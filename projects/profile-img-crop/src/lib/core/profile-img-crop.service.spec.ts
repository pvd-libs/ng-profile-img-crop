import { TestBed } from '@angular/core/testing';

import { ProfileImgCropService } from './profile-img-crop.service';

describe('ProfileImgCropService', () => {
  let service: ProfileImgCropService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProfileImgCropService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
