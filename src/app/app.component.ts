import {Component} from '@angular/core';

@Component({
  selector: 'ng-profile-img-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'image-crop-lib';
  file: File | null = null;
  croppedFile: File | null = null;
  s: string | null = null;

  handleImage(event: Event): void {
    const target: HTMLInputElement = event?.target as HTMLInputElement;

    if (!target?.files?.length) {
      return;
    }

    this.file = target?.files[0];
  }

  onFileEdited(file: File): void {
    this.croppedFile = file;
    let s = URL.createObjectURL(file);
    this.s = s;
  }
}
