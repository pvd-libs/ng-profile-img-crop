import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {ProfileImgCropComponent} from '@profile-img-crop';
import {AppComponent} from './app.component';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [AppComponent],
  imports: [CommonModule, BrowserModule, ProfileImgCropComponent],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
