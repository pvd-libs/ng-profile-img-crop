# ImageCropLib

This project is a simple, lightweight, and easy to use library for cropping images in Android apps. It allows to crop images
for a profile image. The angular app is simple there just to use and test the library.

## LIB
The library uses entirely the browser's canvas api to manipulate the image.

### Features

- Crop image
- Zoom in/out
- Rotate image
- Flip image
- Reset image


### Usage

//TODO add usage note
